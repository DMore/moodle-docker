FROM registry.gitlab.com/dmore/docker-chrome-headless:7.0

RUN apt-get -y update && apt-get -y install libxml2-dev libicu-dev \
 && docker-php-ext-install zip opcache xmlrpc mysqli intl

COPY files/vhost.conf /etc/nginx/sites-enabled/vhost.conf

RUN mkdir /moodledata && chown www-data /moodledata

RUN apt-get -y update && apt-get -y install openjdk-7-jre-headless iceweasel

RUN mkdir -p /usr/lib/selenium \
 && curl -o /usr/lib/selenium/selenium-server-standalone.jar http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar

COPY files/supervisord.conf /etc/supervisord.extra.conf

ENV CHROME_DRIVER_VERSION=2.25

#==================
# Chrome webdriver
#==================
RUN wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
  && rm -rf /opt/selenium/chromedriver \
  && unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
  && rm /tmp/chromedriver_linux64.zip \
  && mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && chmod 755 /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && ln -fs /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver

#========================
# Selenium Configuration
#========================
COPY files/selenium.json /opt/selenium/config.json

RUN cat /etc/supervisord.extra.conf >> /etc/supervisord.conf \
 && echo 'php_admin_flag[xdebug.remote_enable] = Off' >> /usr/local/etc/php-fpm.d/zz-docker.conf

RUN sed -i -e "s/www-data/root/" /usr/local/etc/php-fpm.d/www.conf
